const Plotly_ICI = {
    props: ['chart'],
    render(h) {
        return h('div', {
            attrs: {
                id: this.chart.uuid,
            }
        });
    },

    mounted() {
        this.$nextTick(function () {
        console.log(this.chart.uuid) ; 
        Plotly.react(this.chart.uuid,this.chart.traces,this.chart.layout );
        });
    },

    watch: {
        chart: {
        handler: function() {
         this.$nextTick(function () {
                Plotly.react(this.chart.uuid,this.chart.traces,this.chart.layout );
                console.log("je change !") ; 
            }) ; 
      },
      deep: true
    }
  }

};


var vm = new Vue({
    el: '#app',

    components: {
        'plotly-ici': Plotly_ICI, 
    },

    data() {
        return {
            debug: false,
            app: {
                title: "Simulateur ICI - Etude sur la Covid-19",
                version: 'v0.3.5',
                copyright_owner: "INRIA - Collaboration - IGN",
                github_url: "https://github.com/NicoMaRio67P/CovICI",
                org_url: "https://github.com/NicoMaRio67P/CovICI",
                docs_url: "https://x-ngilet.gitlabpages.inria.fr/html_covici/index.html",
                paper_url: "https://x-ngilet.gitlabpages.inria.fr/html_covici/index.html",
                publisher_url: "https://www.inria.fr/fr/centre-inria-saclay-ile-de-france",
            },
            panel_open: true,
            panel_width: null,
            resizing: false,
            value_ptransmi: 0.015,
            value_duree: 20,
            value_alerte: 200, 
            afficher_graphe: 0, 
            paramError: {},
            running: false,
            indice_graphe: 0, 
            errs: [],
            color_graph: ['#1f77b4','#ff7f0e','#2ca02c','#d62728','#9467bd','#8c564b','#e377c2','#7f7f7f', '#bcbd22','#17becf'],
            rep: [], 
             selected: 'quartier', // Must be an array reference!
            options: [
          { name: 'Quartier', item: 'quartier'},
          { name: 'Flux entrant', item: 'flux_entrant' },
          { name: 'Flux sortant', item: 'flux_sortant'},
          { name: 'Ecolier', item: 'ecolier'},
          { name: 'Etudiant', item: 'etudiant' },
          { name: 'Actif', item: 'actif' },
          { name: 'Retraité', item: 'retraite'}
           ],
            reset_options: ['Sans confinement', 'Confinement global', 'Confinement individualisé'],
            choix_mesure: ['Sans confinement', 'Confinement global', 'Confinement individualisé'],
            choix_indic: ['Nombre total de contaminations', 'Nombre total d\'individus sains','Nombre de nouvelles contaminations','Nombre d\'individus contagieux','Nombre d\'individus guéris','Nombre de nouvelles contaminations (biais)'],
            choix_quartier: ['Paris V - Saint-Victor/Jussieu'],
            choix_duree: ['20'], 
            choix_alerte: ['200'],
            reset_choice: 'Sans confinement',
            value_indic:'Nombre de nouvelles contaminations (biais)', 
            strat_init: 'Sans confinement', 
            cons_graph_status: 'not_accepted', 
            barre_erreur_status: 'accepted', 
            quar_init: 'Paris V - Saint-Victor/Jussieu',
            reset_quar: 'Paris V - Saint-Victor/Jussieu',
            chart: {
                uuid: "graphs",
                traces: [] ,
                layout: {
                    legend: {
                        xanchor:"right",
                        yanchor:"top",
                        x: 1.1,
                        y: 1 
                        },  
                    title:'Principaux indicateurs de la propagation de l\'épidémie de Covid-19',
                    xaxis: {
                        title: 'Nombre de jours'
                    },
                    yaxis: {
                        title: 'Nombre de cas journaliers'
                    }
                }
            }, 
        };
    },


    methods: {

        loadJSON: function() {  
            axios.get('https://gist.githubusercontent.com/NicoMaRio67P/0280661ece29ce75aaf89996ac7a051b/raw/9304308f7d5398ef8e277a51aa3f2eea2be7bfa9/SansMesure.json')
                .then(function (response) {
              console.log(response.data.Nbiais); 
              return(response.data.Nbiais) ; 
                 })    
                 .catch(function (error) {
                 console.log(error);
            });
        }, 

        addData: function() {
            this.chart.traces[0].y.push(Math.random());
        },

        findvalue: function(){
            id = parseInt(this.value_ptransmi/0.005)  ; 
            return(id)
        }, 

        addGraph: function(data1, data2, data3){ 
            this.chart.traces.push({y: data1, name: 'Quantile - 90%', showlegend: false, line: {color:"transparent", width: 2, shape: "line"} }) ; 
            this.chart.traces.push({y: data2, name: this.name_legend(),  fill: 'tonexty', fillcolor:this.hexToRGB(this.color_graph[this.indice_graphe],0.5),line: { width: 2, shape: "line", color:this.hexToRGB(this.color_graph[this.indice_graphe],1) } }) ; 
            this.chart.traces.push({y: data3, name: 'Quantile - 10%', fill: 'tonexty', showlegend: false,  fillcolor: this.hexToRGB(this.color_graph[this.indice_graphe],0.5) ,line: { color:"transparent",width: 2, shape: "line"}  }) ; 
        }, 

        addGraph_serror: function(data){ 
            this.chart.traces.push({y: data, name: 'Moyenne', line: { width: 2, shape: "line", color:this.hexToRGB(this.color_graph[this.indice_graphe],1) } }) ; 
        }, 

        hexToRGB: function (hex, alpha) {
            var r = parseInt(hex.slice(1, 3), 16),
                g = parseInt(hex.slice(3, 5), 16),
                b = parseInt(hex.slice(5, 7), 16);

           if (alpha) {
                return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
            } else {
                return "rgb(" + r + ", " + g + ", " + b + ")";
            }
        }, 

        name_legend: function(){
            return "Moy.- " + this.strat_init + " - ptrans = " + this.value_ptransmi + " - " + this.selected ; 
        },


    computed:{



    }, 
      
    async Afficher_ICI(){
            
            this.running = true ;
            
            try {
                
                this.afficher_graphe = 1 ;

                if(this.cons_graph_status === 'not_accepted'){
                    console.log("Il faut réinitialiser le graphe à O") ; 
                    this.chart.traces = [] ; 
                    this.indice_graphe = 0 ; 
                } else {
                    this.indice_graphe = (this.indice_graphe + 1) % this.color_graph.length ; 

                }; 

                if(this.selected === 'quartier') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/bc96f910afeb10cbb5de04cef0eec5f0/raw/bd239737afbe09dd4555b464c209198c178a3f07/data_total_Stay.json') ; 
                } else if (this.selected === 'flux_entrant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/aa8084f842394af6394c2d44bd0ed187/raw/d7db879f07c6e4cbeb1f075c41ad53803996791e/data_total_In.json') ; 
                }  else if(this.selected === 'flux_sortant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/561f97e01832c23c42e5c48e9e65823d/raw/9bd5c6d167a6f0a0b87c282a56d9194050594677/data_total_Out.json') ; 
                } else if(this.selected === 'ecolier') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/b5c6cb531a038c2738d4a5f31f9d293d/raw/fac574b7f5d29374a28b55ee3f5d245417abd509/data_total_employees.json') ; 
                }  else if (this.selected === 'etudiant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/29186ff3bc2987a04674a31a7e1d5f9a/raw/a5b1fc78d3a5112ab9e5a7a80fda3cfcc63cee31/data_total_students.json') ; 
                }  else if(this.selected === 'actif') {
                     response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/87d063595db9df10034a6430478eca16/raw/3970137589d940b466e7fc9b3343e80e15522570/data_total_pupils.json') ; 
                }  else if(this.selected === 'retraite') {
                     response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/7edeef5e5d4cb11ed15ff84c51ef34d1/raw/37e24789f4c300c3646035edaa6115c738627fe6/data_total_retired.json') ; 
                } ;                

                ident = this.findvalue() ; 

                if(this.value_indic === 'Nombre total de contaminations'){
                    ind = "Ncont" ; 
                    indp = "Ncontp" ; 
                    indm = "Ncontm" ; 
                } else if(this.value_indic === 'Nombre total d\'individus sains') {
                    ind = "Nsain" ; 
                    indp = "Nsainp" ; 
                    indm = "Nsainm" ; 
                } else if(this.value_indic === 'Nombre de nouvelles contaminations') {
                    ind = "Nnewcont" ; 
                    indp = "Nnewcontp" ; 
                    indm = "Nnewcontm" ; 
                } else if(this.value_indic === 'Nombre d\'individus contagieux') {
                    ind = "Ncontamin" ; 
                    indp = "Ncontaminp" ; 
                    indm = "Ncontaminm" ; 
                } else if(this.value_indic === 'Nombre d\'individus guéris') {
                    ind = "Ngueri" ; 
                    indp = "Nguerip" ; 
                    indm = "Nguerim" ; 
                } else {
                    ind = "Nbiais" ;
                    indp = "Nbiaisp" ; 
                    indm = "Nbiaism" ;  
                } ; 

                if(this.barre_erreur_status === 'accepted') {
                    
                    if(this.strat_init === 'Sans confinement') {
                        text = "prob" + String(ident) ; 
                        this.addGraph(response["data"]["SMesure"][`${text}`][`${indp}`],response["data"]["SMesure"][`${text}`][`${ind}`],response["data"]["SMesure"][`${text}`][`${indm}`]); 
                    } else if(this.strat_init === 'Confinement global') {
                        text = "prob" + String(ident) ; 
                        this.addGraph(response["data"]["ATrac"][`${text}`][`${indp}`],response["data"]["ATrac"][`${text}`][`${ind}`],response["data"]["ATrac"][`${text}`][`${indm}`]); 
                    } else if(this.strat_init === 'Confinement individualisé') {
                        text = "prob" + String(ident) ; 
                        this.addGraph(response["data"]["AConf"][`${text}`][`${indp}`],response["data"]["AConf"][`${text}`][`${ind}`],response["data"]["AConf"][`${text}`][`${indm}`]); 
                    } ;

                } else {
                    if(this.strat_init === 'Sans confinement') {
                        text = "prob" + String(ident) ; 
                        this.addGraph_serror(response["data"]["SMesure"][`${text}`][`${ind}`]); 
                    } else if(this.strat_init === 'Confinement global') {
                        text = "prob" + String(ident) ; 
                        this.addGraph_serror(response["data"]["ATrac"][`${text}`][`${ind}`]); 
                    } else if(this.strat_init === 'Confinement individualisé') {
                        text = "prob" + String(ident) ; 
                        this.addGraph_serror(response["data"]["AConf"][`${text}`][`${ind}`]); 
                    }; 

            }; 

                this.chart.layout.title = `Suivi de la propagation de l\'épidémie` ; 
                this.chart.layout.xaxis.title = `Nombre de jours` ; 
                this.chart.layout.yaxis.title = this.value_indic ; 

            } catch (e) {
                console.log("ARF") ; 
                this.errs.push({
                    message: 'Unable to plot the graph.',
                })
                this.panel_open = true
            }
            
            this.running = false ;
            
        },
        
    },

});

