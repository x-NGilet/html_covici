const Plotly_ICI = {
    props: ['chart'],
    render(h) {
        return h('div', {
            attrs: {
                id: this.chart.uuid,
            }
        });
    },

    mounted() {
        this.$nextTick(function () {
        console.log(this.chart.uuid) ; 
        Plotly.react(this.chart.uuid,this.chart.traces,this.chart.layout );
        });
    },

    watch: {
        chart: {
      handler: function() {
         this.$nextTick(function () {
                Plotly.react(this.chart.uuid,this.chart.traces,this.chart.layout );
            }) ; 
      },
      deep: true
    }
  }

};



var vm = new Vue({
    el: '#app',

    components: {
        'plotly-ici': Plotly_ICI, 
    },

    data() {
        return {
            debug: false,
            app: {
                title: "Simulateur ICI - Etude sur la Covid-19",
                version: 'v0.3.5',
                copyright_owner: "INRIA - Collaboration - IGN",
                github_url: "https://github.com/NicoMaRio67P/CovICI",
                org_url: "https://github.com/NicoMaRio67P/CovICI",
                docs_url: "https://x-ngilet.gitlabpages.inria.fr/html_covici/index.html",
                paper_url: "https://x-ngilet.gitlabpages.inria.fr/html_covici/index.html",
                publisher_url: "https://www.inria.fr/fr/centre-inria-saclay-ile-de-france",
                license: 'Loading...',
                notice: 'Loading...'
            },
            panel_open: true,
            panel_width: null,
            resizing: false,
            value_ptransmi: 0.015,
            value_duree: 20,
            value_alerte: 200, 
            afficher_graphe: 0, 
            paramError: {},
            running: false,
            errs: [],
            rep: [], 
            selected: 'quartier', // Must be an array reference!
            options: [
          { name: 'Quartier', item: 'quartier'},
          { name: 'Flux entrant', item: 'flux_entrant' },
          { name: 'Flux sortant', item: 'flux_sortant'},
          { name: 'Ecolier', item: 'ecolier'},
          { name: 'Etudiant', item: 'etudiant' },
          { name: 'Actif', item: 'actif' },
          { name: 'Retraité', item: 'retraite'}
           ],
           selected2: [], // Must be an array reference!
            options2: [
          { text: 'Ecolier', value: 'ecolier', notEnabled: true},
          { text: 'Etudiant', value: 'etudiant', notEnabled: true },
          { text: 'Actif', value: 'actif', notEnabled: true },
          { text: 'Retraité', value: 'retraite', notEnabled: true }
           ],
            reset_options: ['Sans confinement', 'Confinement global', 'Confinement individualisé'],
            choix_mesure: ['Sans confinement', 'Confinement global', 'Confinement individualisé'],
            choix_indic: ['Nombre total de contaminations', 'Nombre total d\'individus sains','Nombre de nouvelles contaminations','Nombre d\'individus contagieux','Nombre d\'individus guéris','Nombre de nouvelles contaminations (biais)'],
            choix_quartier: ['Paris V - Saint-Victor/Jussieu'],
            choix_duree: ['20'], 
            choix_alerte: ['200'],
            reset_choice: 'Sans confinement',
            value_indic:'Nombre de nouvelles contaminations (biais)', 
            strat_init: 'Sans confinement', 
            cons_graph_status: 'not_accepted', 
            barre_erreur_status: 'accepted', 
            quar_init: 'Paris V - Saint-Victor/Jussieu',
            reset_quar: 'Paris V - Saint-Victor/Jussieu',
            chart: {
                uuid: "graphs",
                traces: [
                { 
                 y: [],
                 name: 'Quantile - 90%', 
                line: {
                color:"transparent",
                width: 2,
                shape: "line"
                 }
                },
                { 
                 y: [],
                 name: 'Moyenne',
                 fill: 'tonexty',
                fillcolor: "rgba(0,0,255,0.5)",
                line: {
                width: 2,
                shape: "line",
                color:'blue'
                 }
                },
                { 
                 y: [],
                 name: 'Quantile - 10%',
                fill: 'tonexty',
                fillcolor: "rgba(0,0,255,0.5)",
                line: {
                color:"transparent",
                width: 2,
                shape: "line"
                 }
                }

            ],
                layout: {
                title:'Principaux indicateurs de la propagation de l\'épidémie de Covid-19',
                 xaxis: {
                    title: 'Nombre de jours'
                 },
                yaxis: {
                    title: 'Nombre de cas journaliers'
                }
            }
      }

        };
    },


    methods: {

        loadJSON: function() {
           // var requestURL = 'https://gist.githubusercontent.com/NicoMaRio67P/0280661ece29ce75aaf89996ac7a051b/raw/9304308f7d5398ef8e277a51aa3f2eea2be7bfa9/SansMesure.json';
           // var request = new XMLHttpRequest();
           // request.open('GET', requestURL);
           // request.responseType = 'json';

           // request.onload = function() {
           //     repo = request.response; // get the string from the response
           //     var x = repo.Nbiais ; 
           // }; 
           // request.send();     
            axios.get('https://gist.githubusercontent.com/NicoMaRio67P/0280661ece29ce75aaf89996ac7a051b/raw/9304308f7d5398ef8e277a51aa3f2eea2be7bfa9/SansMesure.json')
                .then(function (response) {
              console.log(response.data.Nbiais); 
              return(response.data.Nbiais) ; 
                 })    
                 .catch(function (error) {
                 console.log(error);
            });

        }, 


        addData: function() {
            this.chart.traces[0].y.push(Math.random());
        },

        findvalue: function(){

            id = parseInt(this.value_ptransmi/0.005)  ; 
            return(id)

        }, 


    computed:{



    }, 

      
    async Afficher_ICI(){
            
            this.running = true ;
            
            try {
                
                this.afficher_graphe = 1 ;
                //for (let i = 0; i < 10000; i++) {
                //    this.chart.traces[0].y[i] = this.value_ptransmi ;
                //}
                console.log(this.selected)
                //for (let i = 0; i < 100; i++) {
                //    this.chart.traces[0].y[i] = Math.random();
                //}; 
                console.log(this.chart.traces[0].y[0]) ; 

                //let response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/24cfd13413fe6d0dea510aad16d376ae/raw/4da5a0c983049c0624c7110e567307059f757d03/data_total.json')

                if(this.selected === 'quartier') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/bc96f910afeb10cbb5de04cef0eec5f0/raw/bd239737afbe09dd4555b464c209198c178a3f07/data_total_Stay.json') ; 
                } else if (this.selected === 'flux_entrant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/aa8084f842394af6394c2d44bd0ed187/raw/d7db879f07c6e4cbeb1f075c41ad53803996791e/data_total_In.json') ; 
                }  else if(this.selected === 'flux_sortant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/561f97e01832c23c42e5c48e9e65823d/raw/9bd5c6d167a6f0a0b87c282a56d9194050594677/data_total_Out.json') ; 
                } else if(this.selected === 'ecolier') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/b5c6cb531a038c2738d4a5f31f9d293d/raw/fac574b7f5d29374a28b55ee3f5d245417abd509/data_total_employees.json') ; 
                }  else if (this.selected === 'etudiant') {
                    response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/29186ff3bc2987a04674a31a7e1d5f9a/raw/a5b1fc78d3a5112ab9e5a7a80fda3cfcc63cee31/data_total_students.json') ; 
                }  else if(this.selected === 'actif') {
                     response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/87d063595db9df10034a6430478eca16/raw/3970137589d940b466e7fc9b3343e80e15522570/data_total_pupils.json') ; 
                }  else if(this.selected === 'retraite') {
                     response = await axios.get('https://gist.githubusercontent.com/NicoMaRio67P/7edeef5e5d4cb11ed15ff84c51ef34d1/raw/37e24789f4c300c3646035edaa6115c738627fe6/data_total_retired.json') ; 
                } ; 


                console.log(response) ; 

                ident = this.findvalue() ; 
                console.log(ident) ; 

                if(this.value_indic === 'Nombre total de contaminations'){
                    ind = "Ncont" ; 
                    indp = "Ncontp" ; 
                    indm = "Ncontm" ; 
                } else if(this.value_indic === 'Nombre total d\'individus sains') {
                    ind = "Nsain" ; 
                    indp = "Nsainp" ; 
                    indm = "Nsainm" ; 
                } else if(this.value_indic === 'Nombre de nouvelles contaminations') {
                    ind = "Nnewcont" ; 
                    indp = "Nnewcontp" ; 
                    indm = "Nnewcontm" ; 
                } else if(this.value_indic === 'Nombre d\'individus contagieux') {
                    ind = "Ncontamin" ; 
                    indp = "Ncontaminp" ; 
                    indm = "Ncontaminm" ; 
                } else if(this.value_indic === 'Nombre d\'individus guéris') {
                    ind = "Ngueri" ; 
                    indp = "Nguerip" ; 
                    indm = "Nguerim" ; 
                } else {
                    ind = "Nbiais" ;
                    indp = "Nbiaisp" ; 
                    indm = "Nbiaism" ;  
                } ; 


                if(this.barre_erreur_status === 'accepted') {
                    
                if(this.strat_init === 'Sans confinement') {
                    text = "prob" + String(ident) ; 
                    console.log(text) ; 
                    this.chart.traces[0].y = response["data"]["SMesure"][`${text}`][`${indp}`] ;
                    this.chart.traces[1].y = response["data"]["SMesure"][`${text}`][`${ind}`] ; 
                    this.chart.traces[2].y = response["data"]["SMesure"][`${text}`][`${indm}`] ; 

 
                } ; 

                if(this.strat_init === 'Confinement global') {
                    text = "prob" + String(ident) ; 
                    this.chart.traces[0].y = response["data"]["ATrac"][`${text}`][`${indp}`] ; 
                    this.chart.traces[1].y = response["data"]["ATrac"][`${text}`][`${ind}`] ; 
                    this.chart.traces[2].y = response["data"]["ATrac"][`${text}`][`${indm}`] ; 

                } ; 

                if(this.strat_init === 'Confinement individualisé') {
                    text = "prob" + String(ident) ; 
                    this.chart.traces[0].y = response["data"]["AConf"][`${text}`][`${indp}`] ; 
                    this.chart.traces[1].y = response["data"]["AConf"][`${text}`][`${ind}`] ; 
                    this.chart.traces[2].y = response["data"]["AConf"][`${text}`][`${indm}`] ; 

                } ; 
            } else {


                if(this.strat_init === 'Sans confinement') {
                    text = "prob" + String(ident) ; 
                    console.log(text) ; 
                    this.chart.traces[0].y = response["data"]["SMesure"][`${text}`][`${ind}`] ;
                    this.chart.traces[1].y =  response["data"]["SMesure"][`${text}`][`${ind}`] ; 
                    this.chart.traces[2].y = undefined ; 

 
                } ; 

                if(this.strat_init === 'Confinement global') {
                    text = "prob" + String(ident) ; 
                    this.chart.traces[0].y = response["data"]["AConf"][`${text}`][`${ind}`] ; 
                    this.chart.traces[1].y = response["data"]["AConf"][`${text}`][`${ind}`]; 
                    this.chart.traces[2].y = undefined ; 

                } ; 

                if(this.strat_init === 'Confinement individualisé') {
                    text = "prob" + String(ident) ; 
                    this.chart.traces[0].y = response["data"]["ATrac"][`${text}`][`${ind}`] ; 
                    this.chart.traces[1].y = response["data"]["ATrac"][`${text}`][`${ind}`]; 
                    this.chart.traces[2].y = undefined ; 

                } ; 


            } ; 

                this.chart.layout.title = `Résultat de la propagation de l\'épidémie pour un ${this.strat_init}` ; 
                this.chart.layout.xaxis.title = `Nombre de jours` ; 
                this.chart.layout.yaxis.title = this.value_indic ; 

                Plotly.purge(this.chart.uuid) ; 
                console.log(this.chart.uuid) ; 

            } catch (e) {
                console.log("ARF") ; 
                this.errs.push({
                    message: 'Unable to plot the graph.',
                })
                this.panel_open = true
            }
            
            this.running = false ;
            
        },
        
    },

});


