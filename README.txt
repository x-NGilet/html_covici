Le site web est défini par l'ensemble des fichiers html (présents dans le dossier principal), 
mais également par les images (présents dans le dossier images) ainsi que 
les fichiers CSS et Javascript (présents dans le dossier assets). 

Le fichier html principal est index.html. 

L'interface utilisateur est définie par 3 fichiers : 
index_app_v2.html (présent dans le dossier principal), app_ici.css (présent dans assets/css/) et par 
app_ici_v2.js (présent dans assets/js/)

Pour mettre à jour le site web avec les corrections, il suffit d'inclure les nouveaux fichiers (s'il y a) 
dans le fichier yml d'intégration continue. 
En réalisant le commit, le site web se mettra à jour automatiquement.

Le template graphique du site web prvient de html5up : 

Hyperspace by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


So I've had the wireframe for this particular design kicking around for some time, but with all
the other interesting (and in some cases, semi-secret) projects I've been working on it took me
a little while to get to actually designing and coding it. Fortunately, things have eased up
enough for me to finaly get around to it, so I'm happy to introduce Hyperspace: a fun, blocky,
one-page design with a lot of color, a bit of animation, and an additional "generic" page template
(because hey, even one-page sites usually need an interior page or two). Hope you dig it :)

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = not included)

AJ
aj@lkn.io | @ajlkn


Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fontawesome.io)

	Other:
		jQuery (jquery.com)
		Scrollex (github.com/ajlkn/jquery.scrollex)
		Responsive Tools (github.com/ajlkn/responsive-tools)
